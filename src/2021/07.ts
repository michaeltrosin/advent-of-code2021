import Day from '@/day';

const Day7: Day = {
  dayNumber: 7,
  solvePart1(filedata: string): number {
    const dists = filedata.split(',').map(n => Number(n));
    const max = Math.max(...dists);
    const min = Math.min(...dists);

    let minSum = 999999999999;

    for (let j = min; j < max; j++) {
      let sum = 0;
      for (let i = 0; i < dists.length; i++) {
        const dist = Math.abs(j - dists[i]);
        sum += dist;
      }
      minSum = Math.min(minSum, sum);
    }

    return minSum;
  },
  solvePart2(filedata: string): number {
    const dists = filedata.split(',').map(n => Number(n));
    const max = Math.max(...dists);
    const min = Math.min(...dists);

    const sumUpTo = (num: number) => {
      let result = 0;
      for (let i = 1; i <= num; i++) {
        result += i;
      }
      return result;
    };

    let minSum = 999999999999;

    for (let j = min; j < max; j++) {
      let sum = 0;
      for (let i = 0; i < dists.length; i++) {
        const dist = Math.abs(dists[i] - j);
        sum += sumUpTo(dist);
      }
      minSum = Math.min(minSum, sum);
    }

    return minSum;
  },
};

export default Day7;
