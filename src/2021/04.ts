import Day from '@/day';

class Board {
  private readonly rows: Array<Array<number>> = [];
  private readonly columns: Array<Array<number>> = [];

  private markedRows: Array<Array<number>> = [];
  private markedColumns: Array<Array<number>> = [];

  public lastDrawnNumber = -1;

  constructor(data: string) {
    this.rows = data.split('\n').map(val => {
      return val.split(' ').filter(n => n !== '').map(n => Number(n));
    });

    for (let i = 0; i < 5; i++) {
      const column: Array<number> = [];
      for (let j = 0; j < 5; j++) {
        column.push(this.rows[j][i]);
      }
      this.columns.push(column);

      this.markedRows.push([]);
      this.markedColumns.push([]);
    }
  }

  public mark(nr: number): boolean {
    for (let i = 0; i < this.rows.length; i++) {
      const row = this.rows[i];
      for (const n of row) {
        if (nr === n) {
          this.markedRows[i].push(n);
          if (this.markedRows[i].length === 5) {
            return true;
          }
        }
      }
    }
    for (let i = 0; i < this.columns.length; i++) {
      const column = this.columns[i];
      for (const n of column) {
        if (nr === n) {
          this.markedColumns[i].push(n);
          if (this.markedColumns[i].length === 5) {
            return true;
          }
        }
      }
    }
    return false;
  }

  public unmarkedNumbers(): Array<number> {
    const marked = this.markedRows.flat();
    return this.rows.flat().filter(val => {
      return marked.indexOf(val) === -1;
    });
  }
}

const Day4: Day = {
  dayNumber: 4,
  solvePart1(filedata: string): number {
    const data = filedata.split('\n\n');

    const bingoNumbers = data.splice(0, 1)[0].split(',').map(n => Number(n));

    const boards: Array<Board> = [];

    data.forEach(val => {
      boards.push(new Board(val));
    });

    for (const nr of bingoNumbers) {
      for (const board of boards) {
        if (board.mark(nr)) {
          board.lastDrawnNumber = nr;
          let result = 0;
          board.unmarkedNumbers().forEach(n => {
            result += n;
          });
          return result * nr;
        }
      }
    }
    return -1;
  },
  solvePart2(filedata: string): number {
    const data = filedata.split('\n\n');

    const bingoNumbers = data.splice(0, 1)[0].split(',').map(n => Number(n));

    const boards: Array<Board> = [];

    data.forEach(val => {
      boards.push(new Board(val));
    });

    const won: Array<Board> = [];

    for (const nr of bingoNumbers) {
      for (const board of boards) {
        if (won.indexOf(board) !== -1) {
          continue;
        }

        if (board.mark(nr)) {
          board.lastDrawnNumber = nr;
          won.push(board);
        }
      }
    }

    const lastWon = won.pop();

    if (lastWon === undefined) {
      return -1;
    }

    let result = 0;
    lastWon.unmarkedNumbers().forEach(n => {
      result += n;
    });
    return result * lastWon.lastDrawnNumber;
  },
};

export default Day4;
