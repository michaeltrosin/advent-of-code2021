import Day from '@/day';

class FishBucket {
  private fish: Array<number> = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  constructor(data: Array<number>) {
    for (const fish of data) {
      this.fish[fish]++;
    }
  }

  public step() {
    // Creates the babies of the fish;
    this.fish.push(this.fish.splice(0, 1)[0]);
    // Adds the count of new fish to the fish with the age 6
    this.fish[6] += this.fish[8];
  }

  public get count(): number {
    let result = 0;
    this.fish.forEach(n => (result += n));
    return result;
  }
}

const Day6: Day = {
  dayNumber: 6,
  solvePart1(filedata: string): number {
    const days = 80;
    const fishBucket = new FishBucket(filedata.split(',').map(n => Number(n)));
    for (let i = 0; i < days; i++) {
      fishBucket.step();
    }
    return fishBucket.count;
  },
  solvePart2(filedata: string): number {
    const days = 256;
    const fishBucket = new FishBucket(filedata.split(',').map(n => Number(n)));
    for (let i = 0; i < days; i++) {
      fishBucket.step();
    }
    return fishBucket.count;
  },
};

export default Day6;
