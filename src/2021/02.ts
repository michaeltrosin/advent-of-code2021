import Day from '@/day';

const Day2: Day = {
  dayNumber: 2,
  solvePart1(filedata: string): number {
    const content = filedata.split('\n');

    let depth = 0;
    let horizontal = 0;

    for (const line of content) {
      const a = line.split(' ');
      if (a[0] === 'forward') {
        horizontal += Number(a[1]);
      } else if (a[0] === 'up') {
        depth -= Number(a[1]);
      } else if (a[0] === 'down') {
        depth += Number(a[1]);
      }
    }

    return depth * horizontal;
  },
  solvePart2(filedata: string): number {
    const content = filedata.split('\n');

    let depth = 0;
    let horizontal = 0;

    let aim = 0;

    for (const line of content) {
      const a = line.split(' ');
      if (a[0] === 'forward') {
        horizontal += Number(a[1]);
        depth += Number(a[1]) * aim;
      } else if (a[0] === 'up') {
        aim -= Number(a[1]);
      } else if (a[0] === 'down') {
        aim += Number(a[1]);
      }
    }
    return depth * horizontal;
  },
};

export default Day2;
