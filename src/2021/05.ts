import Day from '@/day';
import * as fs from 'fs';

class Vec2 {
  public x: number;
  public y: number;

  constructor(raw: string) {
    this.x = Number(raw.split(',')[0]);
    this.y = Number(raw.split(',')[1]);
  }

  public get width(): number {
    return this.x;
  }

  public get height(): number {
    return this.y;
  }
}

const getPointsBetween = (from: Vec2, to: Vec2): Array<Vec2> => {
  const result: Array<Vec2> = [];
  const xCoords: Array<number> = [];
  const yCoords: Array<number> = [];

  const xDist = Math.abs(to.x - from.x);
  const yDist = Math.abs(to.y - from.y);

  for (let i = 0; i <= xDist; i++) {
    if (to.x < from.x) {
      xCoords.push(from.x - i);
    } else {
      xCoords.push(i + from.x);
    }
  }

  for (let i = 0; i <= yDist; i++) {
    if (to.y < from.y) {
      yCoords.push(from.y - i);
    } else {
      yCoords.push(i + from.y);
    }
  }

  if (xDist === 0 && yDist !== 0) {
    for (let i = 0; i < yDist; i++) {
      xCoords.push(xCoords[0]);
    }
  } else if (xDist !== 0 && yDist === 0) {
    for (let i = 0; i < xDist; i++) {
      yCoords.push(yCoords[0]);
    }
  }

  for (let i = 0; i < xCoords.length; i++) {
    result.push(new Vec2(`${xCoords[i]},${yCoords[i]}`));
  }

  return result;
};

const index = (x: number, y: number, width: number) => {
  return y * width + x;
};

const printBoard = (data: Array<number>, size: Vec2) => {
  for (let j = 0; j < size.height; j++) {
    let line = '';
    for (let i = 0; i < size.width; i++) {
      if (data[index(i, j, size.width)] === 0) {
        line += '.';
      } else {
        line += +data[index(i, j, size.width)];
      }
    }
    console.log(line);
  }
};

const Day5: Day = {
  dayNumber: 5,
  solvePart1(filedata: string): number {
    const data = filedata.split('\n').map(line => {
      const lineArr = line.split('->').filter(n => n !== '');

      const from = new Vec2(lineArr[0]);
      const to = new Vec2(lineArr[1]);
      return { from, to };
    });

    const calculateBoardSize = () => {
      let width = 0;
      let height = 0;

      for (const { from, to } of data) {
        width = Math.max(from.x, to.x, width);
        height = Math.max(from.y, to.y, height);
      }

      return new Vec2(`${width + 1},${height + 1}`);
    };

    const boardSize = calculateBoardSize();
    const boardData: Array<number> = new Array<number>((boardSize.x) * (boardSize.y));
    for (let i = 0; i < boardData.length; i++) {
      boardData[i] = 0;
    }

    for (const { from, to } of data) {
      if (((from.x === to.x) || (from.y === to.y))) {
        for (const point of getPointsBetween(from, to)) {
          boardData[index(point.x, point.y, boardSize.width)]++;
        }
      }
    }
    let overlapping = 0;
    for (const i of boardData) {
      if (i > 1) {
        overlapping++;
      }
    }

    // printBoard(boardData, boardSize);
    return overlapping;
  },
  solvePart2(filedata: string): number {
    const data = filedata.split('\n').map(line => {
      const lineArr = line.split('->').filter(n => n !== '');

      const from = new Vec2(lineArr[0]);
      const to = new Vec2(lineArr[1]);
      return { from, to };
    });

    const calculateBoardSize = () => {
      let width = 0;
      let height = 0;

      for (const { from, to } of data) {
        width = Math.max(from.x, to.x, width);
        height = Math.max(from.y, to.y, height);
      }

      return new Vec2(`${width + 1},${height + 1}`);
    };

    const boardSize = calculateBoardSize();
    const boardData: Array<number> = new Array<number>((boardSize.x) * (boardSize.y));
    for (let i = 0; i < boardData.length; i++) {
      boardData[i] = 0;
    }

    for (const { from, to } of data) {
      for (const point of getPointsBetween(from, to)) {
        boardData[index(point.x, point.y, boardSize.width)]++;
      }
    }
    let overlapping = 0;
    for (const i of boardData) {
      if (i > 1) {
        overlapping++;
      }
    }

    // printBoard(boardData, boardSize);
    return overlapping;
  },
};

export default Day5;
