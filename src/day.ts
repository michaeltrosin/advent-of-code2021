import * as fs from 'fs';

interface Day {
  dayNumber: number;
  solvePart1(filedata: string): number;
  solvePart2(filedata: string): number;
}

const solvePart1 = (day: Day, path: string) => {
  return day.solvePart1(fs.readFileSync(`${path}/${day.dayNumber}.txt`, 'utf-8'));
};

const solvePart2 = (day: Day, path: string) => {
  return day.solvePart2(fs.readFileSync(`${path}/${day.dayNumber}.txt`, 'utf-8'));
};

export default Day;
export { solvePart1, solvePart2 };
